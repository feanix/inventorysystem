﻿using System.Collections;
using System.Collections.Generic;
using Assets.Core;
using UnityEngine;

public class Core : MonoBehaviour {

	[SerializeField] private InventoryItem _itemPrefab;
	[SerializeField] private InventoryView _loot;

	private List<InventoryItem> _inventory = new List<InventoryItem>();

    void Awake() {
        Catalogue.init(null);
		initInventory();
    }

	private void initInventory() {
		InventoryItem item;
		GameObject gameObject;
		for (int i = 0; i < 41; i++) {
			item = Instantiate(_itemPrefab, transform, false);
			item.init(i + 1, 1);
			_inventory.Add(item);
			_loot.addItem(item);
		}
	}

}
