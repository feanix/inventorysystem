﻿using UnityEngine.Events;

namespace Assets.Utils {

	public class DragAndDropEvent : UnityEvent<InventoryItem> {

	}

}
