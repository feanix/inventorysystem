﻿using System.Collections;
using System.Collections.Generic;
using Assets.Utils;
using UnityEngine;

public class DragAndDropSystem : MonoBehaviour {

	private static DragAndDropSystem _instance;

	private static InventoryItem _item;
	private static Transform _parent;
	private static Vector3 _position;

	public static readonly DragAndDropEvent onStartDrag = new DragAndDropEvent();
	public static readonly DragAndDropEvent onEndDrag = new DragAndDropEvent();

	[SerializeField] private Transform _canvas;

	public static InventoryItem item {
		get { return _item; }
	}

	public static void startDrag(InventoryItem item) {
		if (_item == item)
			return;
		endDrag();
		_item = item;
		_position = _item.transform.position;
		_parent = _item.transform.parent;
		_item.transform.SetParent(_instance.transform, true);
		onStartDrag.Invoke(_item);
	}

	public static void clear() {
		_item = null;
		_parent = null;
		onEndDrag.Invoke(null);
	}

	public static void endDrag() {
		if (_item != null) {
			_item.transform.SetParent(_parent);
			_item.transform.position = _position;
			onEndDrag.Invoke(_item);
			_item.slot.updateItemSize(_item);
			_item = null;
			_parent = null;
		}
	}

	void Awake() {
		_instance = this;
	}

}
