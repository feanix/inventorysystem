﻿using Assets.Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour, IDragHandler, IEndDragHandler {

	[SerializeField] private Transform _smallIconContainer;
	[SerializeField] private Transform _bigIconContainer;
	[SerializeField] private Text _numberField;

	private StaticItem _staticData;
	private bool _miniature = true;
	private int _number;

	private InventoryGridItem _slot;

	public InventoryGridItem slot {
		get { return _slot; }
		set {
			if (_slot != null)
				_slot.removeItem(this);
			_slot = value;
		}
	}

	public void OnDrag(PointerEventData data) {
		DragAndDropSystem.startDrag(this);
		Vector3 screenPoint = Input.mousePosition;
		screenPoint = Camera.main.ScreenToWorldPoint(screenPoint);
		screenPoint.x += 0.6f;
		screenPoint.y -= 0.6f;
		screenPoint.z = transform.position.z;
		transform.position = screenPoint;
		miniature = false;
	}

	public void OnEndDrag(PointerEventData data) {
		DragAndDropSystem.endDrag();
	}

	public void init(int id, int number) {
		_staticData = StaticDataSingleton.getItem(id);
		if (_staticData.stack < 2)
			_numberField.transform.parent.gameObject.SetActive(false);
		this.number = number;
		updateIcon();
	}

	public StaticItem staticData {
		get { return _staticData; }
	}

	public int number {
		get { return _number; }
		set {
			_number = value;
			_numberField.text = _number.ToString();
		}
	}

	public bool miniature {
		get { return _miniature; }
		set {
			if (_miniature == value)
				return;
			_miniature = value;
			updateIcon();
		}
	}

	private void updateIcon() {
		clearIcons();
		if (_miniature) {
			Instantiate(_staticData.getIconPrefab(), _smallIconContainer, false);
		} else {
			Instantiate(_staticData.getBigIconPrefab(), _bigIconContainer, false);
		}
	}

	private void clearIcons() {
		int children;
		Transform child;
		children = _smallIconContainer.childCount;
		for (int i = children - 1; i >= 0; i--) {
			child = _smallIconContainer.GetChild(i);
			Destroy(child.gameObject);
		}
		children = _bigIconContainer.childCount;
		for (int i = children - 1; i >= 0; i--) {
			child = _bigIconContainer.GetChild(i);
			Destroy(child.gameObject);
		}
	}

}
