﻿namespace Assets.Inventory.Scripts {

	public enum SlotPermissionsType {
		ReadWrite = 0,
		Read = 1,
		Write = 2
	}

}