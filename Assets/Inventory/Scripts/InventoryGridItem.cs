﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Inventory.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryGridItem : MonoBehaviour, IDropHandler {

	[SerializeField] private List<InventorySlotType> _type = null;
	[SerializeField] private SlotPermissionsType _permissions = SlotPermissionsType.ReadWrite;
	[SerializeField] private bool _highlited = false;
	[SerializeField] private GameObject _highlight;

	private InventoryItem _item = null;

	public virtual InventoryItem item {
		get { return _item; }
		set {
			_item = value;
			if (_item != null) {
				_item.miniature = true;
				_item.slot = this;
				_item.transform.SetParent(transform, false);
				_item.transform.localPosition = Vector3.zero;
			}
		}
	}

	public virtual void removeItem(InventoryItem item) {
		this.item = null;
	}

	public virtual void updateItemSize(InventoryItem item) {
		item.miniature = true;
	}

	public virtual void OnDrop(PointerEventData data) {
		if (_permissions == SlotPermissionsType.Read)
			return;
		if ((item == null) && checkItem(DragAndDropSystem.item)) {
			item = DragAndDropSystem.item;
			DragAndDropSystem.clear();
		}
	}

	protected virtual bool checkItem(InventoryItem item) {
		if (type.Count == 0)
			return true;
		foreach (InventorySlotType slotType in type) {
			if (item.staticData.types.IndexOf(slotType) >= 0) {
				return true;
			}
		}
		return false;
	}

	public bool highlited {
		get { return _highlited; }
		set {
			if (_highlight == value)
				return;
			_highlited = value;
			if (_highlight) {
				addDragListeners();
			} else {
				removeDragListeners();
			}
		}
	}

	private void onStartDrag(InventoryItem item) {
		if (!_highlight.activeSelf && checkItem(item))
			_highlight.SetActive(true);
	}

	private void onEndDrag(InventoryItem item) {
		_highlight.SetActive(false);
	}

	private void addDragListeners() {
		DragAndDropSystem.onStartDrag.AddListener(onStartDrag);
		DragAndDropSystem.onEndDrag.AddListener(onEndDrag);
	}

	private void removeDragListeners() {
		DragAndDropSystem.onStartDrag.RemoveListener(onStartDrag);
		DragAndDropSystem.onEndDrag.RemoveListener(onEndDrag);
	}

	public List<InventorySlotType> type {
		get { return _type; }
		set { _type = value.ToList(); }
	}

	public SlotPermissionsType permissions {
		get { return _permissions; }
		set { _permissions = value; }
	}

	protected virtual void Awake() {
		if (_highlited) {
			removeDragListeners();
			addDragListeners();
		}
	}

	void OnDestroy() {
		removeDragListeners();
	}

}
