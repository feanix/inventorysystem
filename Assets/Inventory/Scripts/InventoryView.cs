﻿using System.Collections.Generic;
using System.Linq;
using Assets.Inventory.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class InventoryView : MonoBehaviour {

	private const int CAPACITY = 120;

	[SerializeField] private GridLayoutGroup _grid;
	[SerializeField] private InventoryGridItem _gridItem;

	[SerializeField] private List<InventorySlotType> _type = null;
	[SerializeField] private SlotPermissionsType _permissions = SlotPermissionsType.ReadWrite;

	private List<InventoryGridItem> _gridItems = new List<InventoryGridItem>();

	public void addItem(InventoryItem item) {
		init();
		foreach (InventoryGridItem gridItem in _gridItems) {
			if (gridItem.item == null) {
				gridItem.item = item;
				break;
			}
		}
	}

	public List<InventorySlotType> type {
		get { return _type; }
		set {
			_type = value.ToList();
			foreach (InventoryGridItem item in _gridItems) {
				item.type = _type;
			}
		}
	}

	public SlotPermissionsType permissions {
		get { return _permissions; }
		set {
			_permissions = value;
			foreach (InventoryGridItem item in _gridItems) {
				item.permissions = _permissions;
			}
		}
	}

	void Awake() {
		init();
	}

	private bool _inited = false;
	private void init() {
		if (!_inited) {
			_inited = true;
			for (int i = _grid.transform.childCount - 1; i >= 0; i--) {
				Transform child = _grid.transform.GetChild(i);
				Destroy(child.gameObject);
			}
			for (int i = 0; i < CAPACITY; i++) {
				_gridItems.Add(newGridItem);
			}
		}
	}

	public InventoryGridItem newGridItem {
		get {
			InventoryGridItem result;
			result = Instantiate(_gridItem, _grid.transform, false);
			result.permissions = permissions;
			result.type = type;
			return result;
		}
	}

}
