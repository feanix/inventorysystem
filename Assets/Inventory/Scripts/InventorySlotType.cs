﻿namespace Assets.Inventory.Scripts {

	public enum InventorySlotType {
		Consumable = 1,
		Weapon = 2,
		Material = 3,
		Attachment = 4,
		Easy_Access = 5,
		Armor = 6,
		Helmet = 7,
		Vest = 8,
		Headset = 9,
		Grenade = 10,
		Backpack = 11,
		Shoes = 12,
		Money = 13,
		Ammunition = 14
	}

}
