﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Core;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Inventory.Scripts {

	class InventoryMesh : InventoryGridItem {

		[SerializeField] private ItemSize size = new ItemSize();

		private List<List<InventoryItem>> mesh = new List<List<InventoryItem>>();
		private Dictionary<InventoryItem, List<ItemSize>> index = new Dictionary<InventoryItem, List<ItemSize>>();

		public override InventoryItem item {
			set { }
		}

		public void addItem(InventoryItem item, ItemSize position) {
			List<ItemSize> slots = new List<ItemSize>();
			removeItem(item);
			if (!index.ContainsKey(item))
				index.Add(item, slots);
			else
				index[item] = slots;
			for (int i = position.w; i < position.w + item.staticData.size.w; i++) {
				for (int j = position.h; j < position.h + item.staticData.size.h; j++) {
					mesh[i][j] = item;
					slots.Add(new ItemSize(i, j));
				}
			}
			item.slot = this;
			item.miniature = false;
			item.transform.SetParent(transform);
			item.transform.localPosition = new Vector3(position.w*100 + 50, -position.h*100 - 50, 0); 
		}

		public override void updateItemSize(InventoryItem item) {
			item.miniature = false;
		}

		public override void removeItem(InventoryItem item) {
			if (index.ContainsKey(item)) {
				foreach (ItemSize position in index[item])
					mesh[position.w][position.h] = null;
				index.Remove(item);
				item.slot = null;
			}
		}

		public override void OnDrop(PointerEventData data) {
			InventoryItem item;
			if (permissions == SlotPermissionsType.Read)
				return;
			item = DragAndDropSystem.item;
			Vector3 localPosition = transform.InverseTransformPoint(Camera.main.ScreenToWorldPoint(data.position));
			localPosition.x = (int)Math.Floor(localPosition.x / 100);
			localPosition.y = (int)Math.Floor(localPosition.y / 100)*-1 - 1;
			ItemSize position = new ItemSize((int)localPosition.x, (int)localPosition.y);
			if (checkItem(DragAndDropSystem.item, position)) {
				DragAndDropSystem.clear();
				addItem(item, position);
			}
		}

		protected override bool checkItem(InventoryItem item) {
			throw new Exception("Use an overridden method.");
		}

		protected bool checkItem(InventoryItem item, ItemSize position) {
			bool result;
			ItemSize point;
			InventoryItem meshItem;

			if (item == null)
				return false;

			result = false;
			if (type.Count == 0) {
				result = true;
			} else {
				foreach (InventorySlotType slotType in type) {
					if (item.staticData.types.IndexOf(slotType) >= 0) {
						result = true;
						break;
					}
				}
			}

			if (!result)
				return false;

			for (int i = 0; i < item.staticData.size.w; i++) {
				for (int j = 0; j < item.staticData.size.h; j++) {
					point = new ItemSize(position.w + i, position.h + j);
					if (point.w >= size.w)
						return false;
					if (point.h >= size.h)
						return false;
					if (point.w < 0)
						return false;
					if (point.h < 0)
						return false;
					meshItem = mesh[point.w][point.h];
					if (meshItem != null)
						return false;
				}
			}
			return true;
		}

		protected override void Awake() {
			base.Awake();
			List<InventoryItem> column;
			for (int i = 0; i < size.w; i++) {
				column = new List<InventoryItem>();
				mesh.Add(column);
				for (int j = 0; j < size.h; j++) {
					column.Add(null);
				}
			}
		}
	}

}
