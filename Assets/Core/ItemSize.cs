﻿using System;
using UnityEngine;

namespace Assets.Core {

	[Serializable]
	public class ItemSize {

		[SerializeField] public int w = 1;
		[SerializeField] public int h = 1;

		public ItemSize() { }

		public ItemSize(int w, int h) {
			this.w = w;
			this.h = h;
		}

		public ItemSize clone() {
			return new ItemSize(w, h);
		}

		public override string ToString() {
			return "(W:" + w + "; " + "H:" + h + ")";
		}
	}

}
