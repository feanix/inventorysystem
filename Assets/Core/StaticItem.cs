﻿using System;
using System.Collections.Generic;
using System.IO;
using Assets.Inventory.Scripts;
using UnityEngine;

namespace Assets.Core {

	[Serializable]
	public class StaticItem {

		[SerializeField] public int id = -1;
		[SerializeField] public string prefab = null;
		[SerializeField] public string name = null;
		[SerializeField] public int stack = 1;
		[SerializeField] public ItemSize size = new ItemSize();
		[SerializeField] public List<InventorySlotType> types = new List<InventorySlotType>();

		private GameObject _icon = null;
		private GameObject _bigIcon = null;

		public void init() {
			AssetBundle asset = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "AssetBundles", "Windows", prefab));
			_icon = asset.LoadAsset<GameObject>("image");
			_bigIcon = asset.LoadAsset<GameObject>("bigimage");
		}

		public GameObject getIconPrefab() {
			return _icon;
		}

		public GameObject getBigIconPrefab() {
			return _bigIcon;
		}

	}

}
