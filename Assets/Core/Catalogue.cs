﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Core {

	public class Catalogue {

		public static void init(Action successHandler) {
			TextAsset SourceFile = (TextAsset)Resources.Load("StaticData", typeof(TextAsset));
			string text = SourceFile.text;
			StaticData StaticData = JsonUtility.FromJson(text, typeof(Core.StaticData)) as StaticData;
			StaticDataSingleton.init(StaticData);
		}

	}

}
