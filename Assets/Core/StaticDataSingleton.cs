﻿using System;
using System.Collections.Generic;

namespace Assets.Core {

	public class StaticDataSingleton {

		private static Dictionary<int, StaticItem> _items = new Dictionary<int, StaticItem>();

		private StaticDataSingleton() { }

		public static void init(StaticData staticData) {
			foreach (StaticItem item in staticData.items) {
				if (!_items.ContainsKey(item.id))
					_items.Add(item.id, item);
				else
					_items[item.id] = item;
				item.init();
			}
		}

		public static StaticItem getItem(int id) {
			if (_items.ContainsKey(id))
				return _items[id];
			return null;
		}

	}

}
