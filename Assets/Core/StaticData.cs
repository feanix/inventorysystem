﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Core {

	[Serializable]
	public class StaticData {

		[SerializeField] public List<StaticItem> items = new List<StaticItem>();

	}

}
